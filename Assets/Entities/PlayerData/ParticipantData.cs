﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Players Data", menuName = "Participant Data / New Participant Data")]
public class ParticipantData : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private Color _color;

    public string Name => _name;
    public Color Color => _color;

    public void SetName(string name)
    {
        _name = name;
    }

    public void SetColor(Color color)
    {
        _color = color;
    }
}
