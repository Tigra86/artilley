﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlayerInput))]
public class PlayerShooter : MonoBehaviour
{
    [SerializeField] private Weapon _currentWeapon;
    [SerializeField] private Aim _aim;
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private WeaponsMenu _weaponsMenu;

    public bool IsShownAim { get; private set; }
    public Weapon CurrentWeapon => _currentWeapon;
    public event UnityAction AimHided;
    public event UnityAction ShootDone;

    private PlayerInput _playerInput;
    private bool _isReadyToShoot;

    private void Start()
    {
        IsShownAim = false;
        _isReadyToShoot = true;
        _playerInput = GetComponent<PlayerInput>();
        _playerInput.DelayedOnPlayer += OnDelayedOnPlayer;
        _weaponsMenu.WeaponIconClicked += SetCurrentWeapon;
    }

    private void OnDisable()
    {
        _playerInput.DelayedOnPlayer -= OnDelayedOnPlayer;
        _weaponsMenu.WeaponIconClicked -= SetCurrentWeapon;
    }

    private void OnDelayedOnPlayer()
    {
        TryShowAim();
    }

    private void TryShowAim()
    {
        if (_isReadyToShoot)
        {
            IsShownAim = true;
            _aim.gameObject.SetActive(true);
        }
    }

    private void SetCurrentWeapon(Weapon weapon)
    {
        _currentWeapon = weapon;
        Debug.Log("TODO: CurrentWeapon:" + _currentWeapon);
    }

    public void Shoot()
    {
        if (_isReadyToShoot)
        {
            _currentWeapon.Shoot(_shootPoint, _aim.transform);
            _isReadyToShoot = false;
            StartCoroutine(StartHideAim());
        }
    }

    private IEnumerator StartHideAim()
    {
        yield return new WaitForSeconds(0.5f);
        HideAim();
    }

    public void HideAim()
    {
        IsShownAim = false;
        _aim.gameObject.SetActive(false);
        AimHided?.Invoke();
        StartCoroutine(ReadyFinishedTurn());
    }

    private IEnumerator ReadyFinishedTurn()
    {
        yield return new WaitForSeconds(5f);
        ShootDone?.Invoke();
        _isReadyToShoot = true;
    }
}
