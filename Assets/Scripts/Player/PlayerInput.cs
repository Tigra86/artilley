﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerMover), typeof(Player), typeof(PlayerMover))]
public class PlayerInput : MonoBehaviour
{
    [SerializeField] private float _disabledOffset = 200;
    [SerializeField] private float _downDelay = 1;
    [SerializeField] private float _deltaTimeBetweenClicks = 0.5f;
    [SerializeField] private Player _player;

    public event UnityAction DelayedOnPlayer;

    private PlayerMover _playerMover;
    private PlayerShooter _playerShooter;
    private float _time;
    private bool _isPointerDownOnPlayer;
    private float _timeDown;

    private void OnEnable()
    {
        _playerMover = GetComponent<PlayerMover>();
        _playerShooter = GetComponent<PlayerShooter>();
        _time = 0;
        _isPointerDownOnPlayer = false;
        _timeDown = 0;
    }

    private void Update()
    {
        if (_player.IsTurnActive == false)
            return;

        if (IsPointerOverUiObject())
            return;

        if (_playerShooter.IsShownAim)
        {
            TryRotate(0.01f);
            TryShoot();
            return;
        }

        if (Input.GetMouseButton(0))
            _playerMover.Move(GetMoveDirection(_disabledOffset));

        if (IsDoubleClick())
            _playerMover.TryJump();

        if (IsDelayDownOnPlayer())
            DelayedOnPlayer?.Invoke();
    }

    private void OnMouseDown()
    {
        _isPointerDownOnPlayer = true;
    }

    private void OnMouseUp()
    {
        _isPointerDownOnPlayer = false;
    }

    private static bool IsPointerOverUiObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
#if !ANDROID
        eventDataCurrentPosition.position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
#else
        eventDataCurrentPosition.position = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
#endif
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        foreach (var raycast in results)
            if (raycast.gameObject.TryGetComponent(out PlayerDataView playerData))
                return false;

        return results.Count > 0;
    }

    private void TryRotate(float disabledOffset)
    {
        _playerMover.TryRotate(GetMoveDirection(disabledOffset));
    }

    private void TryShoot()
    {
        if (Input.GetMouseButtonDown(0))
            _playerShooter.Shoot();
    }

    private Vector2 GetMoveDirection(float disabledOffset)
    {
        if (Input.mousePosition.x < Screen.width / 2 - disabledOffset)
            return Vector2.left;

        if (Input.mousePosition.x > Screen.width / 2 + disabledOffset)
            return Vector2.right;

        if (Input.mousePosition.x > Screen.width / 2 - disabledOffset && Input.mousePosition.x < Screen.width / 2 + disabledOffset)
            return Vector2.zero;

        return Vector2.zero;
    }

    private bool IsDoubleClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_time != 0 && _time < _deltaTimeBetweenClicks)
            {
                _time = 0;
                return true;
            }
            else if (_time > _deltaTimeBetweenClicks)
                _time = 0;
        }
        _time += Time.deltaTime;
        return false;
    }

    private bool IsDelayDownOnPlayer()
    {
        if (_isPointerDownOnPlayer)
        {
            _timeDown += Time.deltaTime;
            if (_timeDown > _downDelay)
            {
                _isPointerDownOnPlayer = false;
                _timeDown = 0;
                return true;
            }
        }
        return false;
    }
}