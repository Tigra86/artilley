﻿using UnityEngine;

[RequireComponent(typeof(PlayerShooter))]
public class Player : Participant
{
    private int _startHealth;
    private PlayerShooter _playerShooter;

    private void Awake()
    {
        _startHealth = Health;
    }

    private void OnEnable()
    {
        _playerShooter = GetComponent<PlayerShooter>();
        _playerShooter.ShootDone += InvokeReadyFinishedTurn;
    }

    private void OnDisable()
    {
        _playerShooter.ShootDone -= InvokeReadyFinishedTurn;
    }

    public void AddRewardHealth()
    {
        ApplyDamage(-_startHealth / 2);
    }
}