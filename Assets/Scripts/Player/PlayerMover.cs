﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(Rigidbody2D), typeof(Animator))]
public class PlayerMover : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;

    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidBody;
    private GroundDetection _groundDetection;
    private Animator _animator;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _rigidBody = GetComponent<Rigidbody2D>();
        _groundDetection = GetComponentInChildren<GroundDetection>();
        _animator = GetComponent<Animator>();
    }

    public void Move(Vector2 direction)
    {
        direction *= _speed;
        direction.y = _rigidBody.velocity.y;
        _rigidBody.velocity = direction;

        if (Mathf.Abs(_rigidBody.velocity.x) > 0)
            _animator.Play("Walk");
        else
            _animator.Play("Idle");

        TryRotate(direction);
    }

    public void TryJump()
    {
        _groundDetection.CheckPlayerIsGrounded();
        if (_groundDetection.IsGrounded)
        {
            var jumpDirection = _spriteRenderer.flipX == false ? new Vector3(_speed, _jumpForce, 1) : new Vector3(-_speed, _jumpForce, 1);
            _rigidBody.AddForce(jumpDirection * _jumpForce * Time.deltaTime, ForceMode2D.Impulse);
        }
    }

    public void TryRotate(Vector3 direction)
    {
        if (direction.x > 0) _spriteRenderer.flipX = true;
        if (direction.x < 0) _spriteRenderer.flipX = false;
    }
}