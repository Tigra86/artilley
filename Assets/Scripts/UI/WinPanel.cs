﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinPanel : MonoBehaviour
{
    [SerializeField] private Button _continueButton;

    private void OnEnable()
    {
        _continueButton.onClick.AddListener(OnContinueButtonClicked);  
    }

    private void OnDisable()
    {
        _continueButton.onClick.RemoveListener(OnContinueButtonClicked);
    }

    private void OnContinueButtonClicked()
    {
        SceneManager.LoadScene(0);
    }
}
