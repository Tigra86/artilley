﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameoverPanel : MonoBehaviour
{
    [SerializeField] private Button _backButton;

    private void OnEnable()
    {
        _backButton.onClick.AddListener(OnNoButtonClicked);
        Time.timeScale = 0;
    }

    private void OnNoButtonClicked()
    {
        SceneManager.LoadScene(0);
    }
}
