﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ColorView : MonoBehaviour
{
    [SerializeField] private Button _colorButton;
    [SerializeField] private GameObject _highlightImage;

    private Color _color;

    public Color Color => _color;
    public event UnityAction<Color, ColorView> ColorButtonClick;

    private void OnEnable()
    {
        _colorButton.onClick.AddListener(OnClickColorButton);
        _highlightImage.SetActive(false);
        _color = Color.black;
    }
    public void Render(Color color)
    {
        _color = color;
        _colorButton.gameObject.GetComponent<Graphic>().color = color;
    }

    private void OnClickColorButton()
    {
        ColorButtonClick?.Invoke(_color, this);
    }

    public void TurnOnHighlightImage()
    {
        _highlightImage.SetActive(true);
    }

    public void TurnOffHighlightImage()
    {
        _highlightImage.SetActive(false);
    }
}

