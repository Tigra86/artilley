﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CanvasController : MonoBehaviour
{
    [SerializeField] private GameObject _gameoverPanel;
    [SerializeField] private GameObject _winPanel;

    private Player _player;

    public event UnityAction<Player> PlayerRevived;
    public bool IsGameoverPanelEnabled { get; private set; }

    private void OnEnable()
    {
        Time.timeScale = 1;
    }

    public void ActivateGameoverPanel(Player player)
    {
        IsGameoverPanelEnabled = true;
        _gameoverPanel.SetActive(true);
        _player = player;
    }

    public void DisableGameoverPanel()
    {
        IsGameoverPanelEnabled = false;
        if (_gameoverPanel != null)
            _gameoverPanel.SetActive(false);
    }

    public void ActivateWinPanel()
    {
        _winPanel.SetActive(true);
        Time.timeScale = 0;
    }

    public void RevivePlayer()
    {
        _player.AddRewardHealth();
        PlayerRevived?.Invoke(_player);
    }
}
