﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(StartPage))]
public class ColorsPanel : MonoBehaviour
{
    [SerializeField] private List<Color> _colors;
    [SerializeField] private ColorView _templateView;
    [SerializeField] private Transform _container;

    public event UnityAction ChangedSelectedColor;
    public bool IsSelectedColor { get; private set; }

    private Color _selectedColor;
    private ColorView _selectedView;
    private StartPage _startPage;

    private void Start()
    {
        _startPage = GetComponent<StartPage>();

        foreach (var color in _colors)
            AddColor(color);
    }

    private void AddColor(Color color)
    {
        var view = Instantiate(_templateView, _container);
        view.Render(color);
        view.ColorButtonClick += OnColorButtonClick;
    }

    private void OnColorButtonClick(Color color, ColorView view)
    {
        if (_selectedView != null)
        {
            SetUnselectedState(_selectedView);
            SetSelectedState(color, view);
        }
        else
        {
            SetSelectedState(color, view);
        }
    }

    private void SetSelectedState(Color color, ColorView view)
    {
        view.TurnOnHighlightImage();
        SetSelectedColor(color);
        _selectedView = view;
    }

    private void SetUnselectedState(ColorView view)
    {
        view.TurnOffHighlightImage();
        SetDefaultColor();
    }

    private void SetSelectedColor(Color color)
    {
        IsSelectedColor = true;
        _selectedColor = color;
        _startPage.PlayerData.SetColor(_selectedColor);
        ChangedSelectedColor?.Invoke();
    }

    private void SetDefaultColor()
    {
        _selectedColor = Color.black;
        _startPage.PlayerData.SetColor(_selectedColor);
        ChangedSelectedColor?.Invoke();
    }
}
