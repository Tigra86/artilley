﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class WeaponsMenu : MonoBehaviour
{
    [SerializeField] private List<Weapon> _weapons;
    [SerializeField] private PlayerShooter _playerShooter;
    [SerializeField] private WeaponView _template;
    [SerializeField] private GameObject _container;
    [SerializeField] private Button _weaponsButton;

    public event UnityAction<Weapon> WeaponIconClicked;

    private void OnEnable()
    {
        CreateWeapons();
        ChangeMenuVisibility();
        _weaponsButton.onClick.AddListener(OnClickWeaponButton);
    }

    private void CreateWeapons()
    {
        foreach (var weapon in _weapons)
            AddWeapon(weapon);
    }

    private void AddWeapon(Weapon weapon)
    {
        var view = Instantiate(_template, _container.transform);
        view.Render(weapon);
        view.IconClicked += OnWeaponIconClicked;
    }

    private void OnClickWeaponButton()
    {
        ChangeMenuVisibility();
    }

    public void ChangeMenuVisibility()
    {
        _container.SetActive(!_container.activeSelf);
    }

    private void OnWeaponIconClicked(Weapon weapon)
    {
        WeaponIconClicked?.Invoke(weapon);
        ChangeMenuVisibility();
    }
}
