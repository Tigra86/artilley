﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class StartPage : MonoBehaviour
{
    [SerializeField] private TMP_InputField nameField;
    [SerializeField] private Button _playButton;
    [SerializeField] private ColorsPanel _colorsPanel;
    [SerializeField] private ParticipantData _playerData;

    public ParticipantData PlayerData => _playerData;

    private void OnEnable()
    {
        ResetPlayerData();
        _playButton.interactable = false;
        nameField.onEndEdit.AddListener(OnEditEndName);
        _playButton.onClick.AddListener(OnClickPlayButton);
        _colorsPanel.ChangedSelectedColor += CheckData;
    }

    private void OnDisable()
    {
        _colorsPanel.ChangedSelectedColor -= CheckData;
    }

    private void OnEditEndName(string name)
    {
        _playerData.SetName(name);
        CheckData();
    }

    private void ResetPlayerData()
    {
        _playerData.SetName(string.Empty);
        _playerData.SetColor(Color.black);
    }

    private void OnClickPlayButton()
    {
        LoadBattleScene();
    }

    private void LoadBattleScene()
    {
        SceneManager.LoadScene(1);
    }

    private void CheckData()
    {
        if (_playerData.Name == string.Empty)
            _playButton.interactable = false;
        else if (_playerData.Color == Color.black)
            _playButton.interactable = false;
        else
            _playButton.interactable = true;
    }
}