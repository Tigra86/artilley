﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class WeaponView : MonoBehaviour
{
    [SerializeField] private Image _selectHighlight;
    [SerializeField] private Button _iconButton;
    [SerializeField] private TMP_Text _label;

    private Weapon _weapon;

    public event UnityAction<Weapon> IconClicked;

    public void Render(Weapon weapon)
    {
        _weapon = weapon;
        _iconButton.image.sprite = weapon.Icon;
        _label.text = weapon.Label;
    }

    private void OnEnable()
    {
        _iconButton.onClick.AddListener(OnClickIconButton);
    }
    private void OnDisable()
    {
        _iconButton.onClick.RemoveListener(OnClickIconButton);
    }

    private void OnClickIconButton()
    {
        IconClicked?.Invoke(_weapon);
    }
}
