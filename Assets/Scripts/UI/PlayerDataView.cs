﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerDataView : MonoBehaviour
{
    [SerializeField] private float _offsetY;
    [SerializeField] private TMP_Text _name;
    [SerializeField] private TMP_Text _health;
    [SerializeField] private Image _pointerImage;

    public void Render(Participant participant)
    {
        _name.text = participant.Name;
        _name.color = participant.Color;
        _health.color = participant.Color;
        _health.text = participant.Health.ToString();
        SetPosition(participant, _offsetY);
    }

    private void SetPosition(Participant participant, float offsetY)
    {
        Vector2 position = participant.transform.position;
        Vector2 positionOnScreen = Camera.main.WorldToScreenPoint(position);
        positionOnScreen.y += offsetY;
        transform.position = positionOnScreen;
    }

    public void ChangePointerVisibility()
    {
        if (_pointerImage != null)
            _pointerImage.gameObject.SetActive(!_pointerImage.gameObject.activeSelf);
    }
}
