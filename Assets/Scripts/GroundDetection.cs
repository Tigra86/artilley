﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDetection : MonoBehaviour
{
    [SerializeField] private LayerMask _mask;
    public bool IsGrounded { get; private set; }

    private float distanceX = 0.25f;
    private float distanceY = 0.7f;
    private float distanceYUp = 0.3f;

    private void Start()
    {
        IsGrounded = false;
    }

    public void CheckPlayerIsGrounded()
    {
        var raycastHits = new List<RaycastHit2D>(Physics2D.RaycastAll(new Vector2(transform.position.x - distanceX, transform.position.y), Vector2.down, distanceY, _mask));
        var raycastHitsRight = new List<RaycastHit2D>(Physics2D.RaycastAll(new Vector2(transform.position.x + distanceX, transform.position.y), Vector2.down, distanceY, _mask));
        raycastHits.AddRange(raycastHitsRight);

        var raycastHitsUp = Physics2D.RaycastAll(transform.position, Vector2.up, distanceYUp);
        if (raycastHits != null)
        {
            foreach (var rh in raycastHits)
            {
                if (rh.transform.gameObject.TryGetComponent(out Ground ground1))
                {
                    if (raycastHitsUp != null)
                    {
                        foreach (var rhu in raycastHitsUp)
                        {
                            if (rhu.transform.gameObject.TryGetComponent(out Ground ground2))
                            {
                                IsGrounded = false;
                                return;
                            }
                        }
                    }
                    IsGrounded = true;
                    return;
                }
            }
        }
        IsGrounded = false;
    }
}

