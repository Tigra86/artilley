﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] private string _label;
    [SerializeField] private Sprite _icon;
    [SerializeField] protected Bullet Bullet;

    public Sprite Icon => _icon;
    public string Label => _label;

    public abstract void Shoot(Transform shootPoint, Transform aimPoint);

}
