﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    public override void Shoot(Transform shootPoint, Transform aimPoint)
    {
        Bullet bullet = Instantiate(Bullet, shootPoint);
        bullet.SetAim(aimPoint);
    }
}
