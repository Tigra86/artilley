﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : Participant
{
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private Player _player;
    [SerializeField] private Weapon _currentWeapon;

    private void OnEnable()
    {
        TurnActivated += OnTurnActivated;
    }

    private void OnDisable()
    {
        TurnActivated -= OnTurnActivated;
    }

    private void OnTurnActivated()
    {
        StartCoroutine(DoShoot(3));
    }

    private IEnumerator DoShoot(float delay)
    {
        yield return new WaitForSeconds(delay);
        Shoot(_shootPoint, _player.transform);

        yield return new WaitForSeconds(delay);
        InvokeReadyFinishedTurn();
    }

    private void Shoot(Transform shootPoint, Transform aimPoint)
    {
        _currentWeapon.Shoot(shootPoint, aimPoint);
    }
}
