﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    [SerializeField] private float _distance;

    private Vector2 _shootPointPosition;

    private void OnEnable()
    {
        FollowMouse();
    }

    private void Update()
    {
        FollowMouse();
    }

    private void FollowMouse()
    {
        _shootPointPosition = transform.parent.gameObject.GetComponentInChildren<ShootPoint>().transform.position;

        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 position = (mousePosition - _shootPointPosition).normalized * _distance;
        position += _shootPointPosition;

        transform.position = position;
    }
}