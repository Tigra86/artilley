﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class PistolBullet : Bullet
{
    private void Update()
    {
        if (Direction == Vector2.zero)
            Direction = ((Vector2)AimPoint.position - (Vector2)transform.position).normalized;

        transform.localRotation = Quaternion.AngleAxis(Vector2.SignedAngle(Vector2.right, Direction), Vector3.forward);
        transform.Translate(Vector3.right * Speed * Time.deltaTime);
    }
}
