﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private int _damage;

    protected Transform AimPoint;
    protected Vector2 Direction;

    public float Speed => _speed;
    public int Damage => _damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Bot bot))
            bot.ApplyDamage(Damage);

        if (collision.gameObject.TryGetComponent(out Player player))
            player.ApplyDamage(Damage);

        Destroy(gameObject);
    }

    public void SetAim(Transform aimPoint)
    {
        AimPoint = aimPoint;
    }

    protected void Explose(float radius)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, radius);

        foreach (var collider in colliders)
        {
            if (collider.gameObject.TryGetComponent(out Bot bot))
                bot.ApplyDamage(Damage);

            if (collider.gameObject.TryGetComponent(out Player player))
                player.ApplyDamage(Damage);
        }
        Destroy(gameObject, 0.1f);
    }
}
