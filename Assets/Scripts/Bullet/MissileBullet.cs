﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Animator))]
public class MissileBullet : Bullet
{
    [SerializeField] private float _delayBeforeRotateDown;

    private Animator _animator;
    private Sequence _flying;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Direction == Vector2.zero)
        {
            Direction = ((Vector2)AimPoint.position - (Vector2)transform.position).normalized * Speed * Time.deltaTime;

            var angle = Vector2.SignedAngle(Vector2.right, Direction);

            _flying = DOTween.Sequence();
            _flying.Append(transform.DOMove(Direction, 1f).SetRelative());
            _flying.Insert(0f, transform.DORotate(new Vector3(0, 0, angle), 0f));

            if (Direction.y > transform.position.y)
            {
                _flying.Append(transform.DOMove(new Vector2(Direction.x, -Direction.y) * 20, 2 * 20f).SetRelative());
                _flying.Insert(0.5f, transform.DORotate(new Vector3(0, 0, -angle), 1f));
            }
            else
            {
                _flying.Append(transform.DOMove(new Vector2(Direction.x, Direction.y) * 20, 2 * 20f).SetRelative());
                _flying.Insert(0.5f, transform.DORotate(new Vector3(0, 0, angle), 1f));
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Ground ground) || collision.gameObject.TryGetComponent(out Bot bot))
        {
            _flying.Kill();
            _animator.Play("Explose");
        }
    }
}