﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Animator), typeof(SpriteRenderer))]
public class GrenadeBullet : Bullet
{
    [SerializeField] private Rigidbody2D _rigidBody;
    [SerializeField] private float _jumpForce;
    [SerializeField] private int _jumpCount;
    [SerializeField] private float _jumpSpreading;
    [SerializeField] private float _delay;

    private Vector2 _directionJump;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Direction == Vector2.zero)
        {
            Direction = ((Vector2)AimPoint.position - (Vector2)transform.position).normalized;
            _rigidBody.AddForce(Direction * Speed * Time.deltaTime, ForceMode2D.Impulse);
            StartCoroutine(DoExplose());
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent(out Ground ground))
        {
            _directionJump = new Vector2(Random.Range(Direction.x - _jumpSpreading, Direction.x + _jumpSpreading), Random.Range(Direction.y - _jumpSpreading, Direction.y + _jumpSpreading)).normalized;

            if (_jumpCount > 0)
            {
                _rigidBody.AddForce(_directionJump * _jumpForce * Time.deltaTime, ForceMode2D.Impulse);
                _jumpCount--;
                _jumpForce /= 1.5f;
            }
        }
    }

    private IEnumerator DoExplose()
    {
        yield return new WaitForSeconds(_delay);
        _animator.Play("Explose");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, 5f);
    }
}
