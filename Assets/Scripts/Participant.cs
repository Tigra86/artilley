﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class Participant : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private ParticipantData _participantData;
    [SerializeField] private PlayerDataView _view;

    private string _name;
    private Color _color;
    private Animator _animator;

    public int Health => _health;
    public string Name => _name;
    public Color Color => _color;
    public bool IsTurnActive { get; protected set; }
    public event UnityAction<int> HealthChanged;
    public event UnityAction<Participant> PlayerDied;
    public event UnityAction<Participant> BotDied;
    public event UnityAction ReadyFinishedTurn;
    public event UnityAction TurnActivated;

    private void Start()
    {
        _name = _participantData.Name;
        _color = _participantData.Color;
        _view.Render(this);
        HealthChanged?.Invoke(Health);
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        _view.Render(this);
    }

    public void DoActive()
    {
        IsTurnActive = true;
        TurnActivated?.Invoke();
        _view.ChangePointerVisibility();
        StartCoroutine(HidePointer(2));
    }

    public void DoInactive()
    {
        IsTurnActive = false;
    }

    protected void InvokeReadyFinishedTurn()
    {
        ReadyFinishedTurn?.Invoke();
        DoInactive();
    }

    public virtual void ApplyDamage(int damage)
    {
        _health -= damage;
        HealthChanged?.Invoke(_health);
        _view.Render(this);

        if (damage > 0)
            _animator.Play("ApplyDamage");

        if (_health <= 0)
            StartCoroutine(StartDie());
    }
    private IEnumerator StartDie()
    {
        _health = 0;
        yield return new WaitForSeconds(1);
        Die();
    }

    private void Die()
    {
        if (gameObject.TryGetComponent(out Player player))
        {
            PlayerDied?.Invoke(this);
        }
        else
        {
            BotDied?.Invoke(this);
            Destroy(_view.gameObject);
            Destroy(gameObject);
        }
    }

    private IEnumerator HidePointer(float delay)
    {
        yield return new WaitForSeconds(delay);
        _view.ChangePointerVisibility();
    }

    public void PlayIdleAnimation()
    {
        _animator.Play("Idle");
    }
}
