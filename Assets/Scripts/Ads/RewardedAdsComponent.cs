﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

public class RewardedAdsComponent : MonoBehaviour, IUnityAdsListener
{
#if UNITY_IOS
    private string _gameId = "3568039";
#elif UNITY_ANDROID
    private string _gameId = "4089005";
#elif UNITY_STANDALONE_WIN
    private string _gameId = "4089005";
#endif

    [SerializeField] private CanvasController _canvasController;

    public string MyPlacementId = "rewardedVideo";
    public static bool ListenerSet = false;
    public event Action<string> OnAdsReady;
    public event UnityAction AdWatched;

    void Awake()
    {
        Advertisement.RemoveListener(this);
        if (!ListenerSet)
        {
            Advertisement.AddListener(this);
            ListenerSet = true;
        }

        Advertisement.Initialize(_gameId, true);
    }

    public void OnUnityAdsReady(string placementId)
    {
        if (placementId == MyPlacementId)
        {
            if (OnAdsReady != null)
            {
                OnAdsReady(placementId);
            }
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            AdWatched?.Invoke();

            if (_canvasController.IsGameoverPanelEnabled == false)
                Debug.Log("TODO: GIVE REWARD!");
            else
            {
                _canvasController.DisableGameoverPanel();
                _canvasController.RevivePlayer();
            }
        }
        else if (showResult == ShowResult.Skipped)
        {
            Debug.Log("Skipped");
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.Log("Failed");
        }
        else
        {
            Debug.Log("Error");
        }
        Time.timeScale = 1;
    }

    public void OnUnityAdsDidError(string message)
    {
    }

    public void OnUnityAdsDidStart(string placementId)
    {
    }
}