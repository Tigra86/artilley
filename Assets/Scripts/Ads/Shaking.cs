﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Shaking : MonoBehaviour
{
    [SerializeField] private RewardedAdsComponent _adComponent;
    [SerializeField] private Button _adButton;
    [SerializeField] private float _shakeDuration;
    [SerializeField] private float _timeBetweenShakings;

    private float _timeLast;

    private void OnEnable()
    {
        _adButton.onClick.AddListener(OnClickAdButton);
        _adComponent.AdWatched += ResetTime;
    }

    private void OnDisable()
    {
        _adComponent.AdWatched -= ResetTime;
    }

    private void Update()
    {
        _timeLast += Time.deltaTime;
        if (_timeLast >= _timeBetweenShakings + _shakeDuration)
        {
            DoShake();
            ResetTime();
        }
    }

    private void OnClickAdButton()
    {
        ResetTime();
    }

    private void ResetTime()
    {
        _timeLast = 0;
    }

    private void DoShake()
    {
        transform.DOShakePosition(_shakeDuration, 10, 20, 90, false);
    }
}

