﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Advertisements;

[RequireComponent(typeof(Button))]
public class RewardedAdsButton : MonoBehaviour
{
    [SerializeField] private RewardedAdsComponent _adComponent;

    private Button _myButton;
    private string _myPlacementId;

    void Start()
    {
        _myButton = GetComponent<Button>();

        _myButton.interactable = Advertisement.IsReady(_myPlacementId);

        if (_myButton)
        {
            _myButton.onClick.AddListener(ShowRewardedVideo);
        }
        _adComponent.OnAdsReady += OnUnityAdsReady;
    }

    void ShowRewardedVideo()
    {
        Time.timeScale = 0;
        Advertisement.Show(_myPlacementId);
    }

    public void OnUnityAdsReady(string placementId)
    {
        _myButton.interactable = true;
    }
}
