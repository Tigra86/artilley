﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle : MonoBehaviour
{
    [SerializeField] private List<Participant> _participants;
    [SerializeField] private CanvasController _canvasController;
    [SerializeField] private Camera _camera;
    [SerializeField] private float _cameraOffsetY;

    private int _currentParticipantIndex;
    private Participant _currentParticipant;
    private Vector3 _cameraPosition;

    public bool IsPlayerAlive { get; private set; }

    private void Start()
    {
        SetCurrentPartipantByIndex(0);
        IsPlayerAlive = true;
    }

    private void OnEnable()
    {
        _canvasController.PlayerRevived += OnPlayerRevived;

        foreach (var participant in _participants)
        {
            participant.ReadyFinishedTurn += TrySetNextParticipent;
            participant.BotDied += OnBotDied;
            participant.PlayerDied += OnPlayerDied;
        }
    }

    private void OnDisable()
    {
        foreach (var participant in _participants)
            participant.ReadyFinishedTurn -= TrySetNextParticipent;
    }

    private void SetCurrentPartipantByIndex(int index)
    {
        if (_currentParticipant != null)
            _currentParticipant.DoInactive();

        _currentParticipantIndex = index;
        _currentParticipant = _participants[index];
        _currentParticipant.DoActive();

        _camera.transform.parent = _currentParticipant.transform;
        _cameraPosition = new Vector3(_currentParticipant.transform.position.x, _currentParticipant.transform.position.y + _cameraOffsetY, -10);
        _camera.transform.position = _cameraPosition;
    }

    private void OnPlayerRevived(Player player)
    {
        IsPlayerAlive = true;
        player.PlayerDied += OnPlayerDied;
        player.ReadyFinishedTurn += TrySetNextParticipent;

        if (IsTurnActivePartipant() == false)
            TrySetNextParticipent();
    }

    private bool IsTurnActivePartipant()
    {
        foreach (var partipant in _participants)
            if (partipant.IsTurnActive)
                return true;

        return false;
    }

    private void TrySetNextParticipent()
    {
        _currentParticipant.DoInactive();

        if (IsPlayerAlive)
        {
            if (_currentParticipantIndex < _participants.Count - 1)
                _currentParticipantIndex++;
            else if (_currentParticipantIndex == _participants.Count - 1)
                _currentParticipantIndex = 0;

            SetCurrentPartipantByIndex(_currentParticipantIndex);
        }
    }

    private void OnBotDied(Participant participant)
    {
        participant.BotDied -= OnBotDied;

        _participants.Remove(participant);

        CheckFinishBattle();
    }

    private void OnPlayerDied(Participant participant)
    {
        participant.PlayerDied -= OnPlayerDied;
        participant.ReadyFinishedTurn -= TrySetNextParticipent;

        IsPlayerAlive = false;

        CheckFinishBattle();
    }

    private void CheckFinishBattle()
    {
        if (IsBotInBattle() == false && (IsPlayerAlive == false))
        {
            Debug.Log("TODO: Ничья.");
        }
        else if (IsBotInBattle() == false)
        {
            _canvasController.ActivateWinPanel();
        }
        else if (IsPlayerAlive == false)
        {
            foreach (var partipant in _participants)
                if (partipant.gameObject.TryGetComponent(out Player player))
                    _canvasController.ActivateGameoverPanel(player);
        }
        else
        {
            //TrySetNextParticipent();
        }
    }

    private bool IsBotInBattle()
    {
        foreach (var partipant in _participants)
            if (partipant.gameObject.TryGetComponent(out Bot bot))
                return true;

        return false;
    }
}
